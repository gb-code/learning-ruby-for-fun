# -----------------------------------------------------------------------
# File Name     	: floor and ceil algorithm.rb
# Created By     	: Gaurav Belwal
#Description   		: Given a sorted array and a value x, Find Ceil(ceiling of x is the smallest element in array greater than or equal to x)
# 					and Floor(the greatest element smaller than or equal to x) of x.
#					Algorithm: Binary-search
#					Time-complexity: O(logn),Auxiliary-space:O(1)
# -----------------------------------------------------------------------

def floor_ceil(array,key)
    size=array.length
    floor=floor_func(array,0,size,key)
    ceil=ceil_func(array,-1,size-1,key)
   
   if floor==-1 puts "floor doesn't exist"
        if ceil==-1 
			puts "Ceil doesn't exist"
            return -1
        else
            return "Ceil is #{array[ceil]}"
        end
    else
        if ceil==-1
            puts "Ceil doesn't exist"
            return "Floor is #{array[floor]}"
        else
            return "Floor is #{array[floor]} and Ceil is #{array[ceil]}"
        end
    end
end

#Function to find floor  
def floor_func(array,lowest,highest,key)
    return -1 if key<array[lowest]
    while highest-lowest>1
        mid=lowest+(highest-lowest)/2
        if array[mid]<=key 
           lowest=mid
        else
            highest=mid
        end
    end
    return lowest
end

#Function to find Ceil
def ceil_func(array,lowest,highest,key)
    return -1 if key>array[highest]
    while highest-lowest>1
        mid=lowest+(highest-lowest)/2
        if array[mid]>=key
            highest=mid
        else
            lowest=mid
        end
    end
    return highest
end
 floor_ceil([1, 2, 8, 10, 10, 12, 19],19) # => Floor is 19 and Ceil is 19