
def detect_cycle(head)
  return false unless head
  slow_ptr = head.head
  fast_ptr = head.head

  while fast_ptr && fast_ptr.next != nil
    slow_ptr = slow_ptr.next
    fast_ptr = fast_ptr.next.next
    if slow_ptr == fast_ptr
      break
    end
  end

  return false if fast_ptr.nil? || fast_ptr.next.nil?

  while slow_ptr != fast_ptr
    slow_ptr = head
    slow_ptr = slow_ptr.next
    fast_ptr = fast_ptr.next
  end

  return fast_ptr
end