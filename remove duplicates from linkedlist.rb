# -----------------------------------------------------------------------
# File Name     	: remove duplicates from linkedlist.rb
# Created By     	: Gaurav Belwal
#Description   		: The program removes duplicates from linkedlist.
# -----------------------------------------------------------------------


def duplicates(parent)
  return false unless parent
  visited = {}

  current = parent
  while current
    if visited[current.val]
      remove(current)
    else
      visited[current.val] = true
      current = current.next
    end
  end
end

def remove(node)
  node.val = node.next.val || nil
  node.next = node.next.next
end