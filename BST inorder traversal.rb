# -----------------------------------------------------------------------
# File Name     	: BST inorder traversal.rb
# Created By     	: Gaurav Belwal
# Description   	: Given a binary tree, return the inorder traversal of its nodes' values.
# -----------------------------------------------------------------------
# class TreeNode
#     attr_accessor :val, :left, :right
#     def initialize(val)
#         @val = val
#         @left, @right = nil, nil
#     end
# end

# @param {TreeNode} parent


def traverse(parent)
  result_array = []
  recursive_func(parent, result_array)
  result_array
end

def recursive_func(parent, result_array)
  return result_array unless parent 
  
  recursive_func(parent.left, result_array) if parent.left
  
  result_array << parent.val
  
  recursive_func(parent.right, result_array) if parent.right
end

def iterative_func(parent)
  
  result, stack = [], [parent]

  while stack.length > 0
    node = stack.pop
    stack.push(node.left) if node.left
    result << node.val
    stack.push(node.right) if node.right
  end

  result
end