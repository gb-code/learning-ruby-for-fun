# -----------------------------------------------------------------------
# File Name     	: graph breadth first search.rb
# Created By     	: Gaurav Belwal
#Description   		: The program implements breadth first searching of graph.
# -----------------------------------------------------------------------

class BFS

  attr_accessor :graph, :start, :visited, :edge_to

  def set(graph, start)
    @graph = graph
    @start = start
    @visited = [@start]
    @edge_to = {}

    bfs
  end

  def bfs
  #idea: to use queue to store all the nodes as we traverse them, marking them visit and enqueuing them
    queue = [@start]

    while queue.length > 0 #while queue is not empty
      node = queue.shift #shifting/popping out to a variable node
      
	  
	  #get all the adjacent vertices of the dequeued vertex node.
	  #if the adjacent item has not been visited, mark it visited and push to the queue.
	  @graph[node].each do |child_node|
        #checking whether visited
		unless @visited.include?(child_node)
          #if not pushed to queue
		  queue.push(child_node)
          #marked visited
		  @visited << child_node
          @edge_to[child_node] = node
        end
      end
    end
  end

  #all helper functions below
  def has_path_to?(v)
    @visited.include?(v)
  end

  def shortest_path(v)
    stack = [v]

    while v != @start
      v = @edge_to[v]
      stack.push(v)
    end

    stack.push(@start)
  end
end