# -----------------------------------------------------------------------
# File Name     	: median of two arrays.rb
# Created By     	: Gaurav Belwal
#Description   		: The program find the median of the array obtained after merging the arrays A(n) and B(n) (i.e. array of length 2n).
# -----------------------------------------------------------------------



def median_func(a,a1,low1,hi1,low2,hi2)
  
    size=hi1-low1+1 #Length of the arrays  
    
    return -1 if size<=0 # return -1 if array is empty or invalid
    return (a[low1]+a1[low2])/2 if  size==1 # return median if each array is of size 1
    return ([a[low1], a1[low2]].max + [a[hi1], a1[hi2]].min)/2 if  size==2 # return median if each array is of size 2
  
    #Median and index of median of array 1
    m1,m1_i=median(a,size,low1)
  
    #Median and index of median of array 2
    m2,m2_i=median(a1,size,low2)
  
    #Compare medians if equal return either m1 or m2,
    #else if m1<m2 median lies in m1,hi1 and low1,m2
    #else m1>m2 median lies in m1,hi1 and low1,m2
    if m1==m2
        return m1
    elsif m1<m2
        return median(a,a1,m1_i,hi1,low2,m2_i)
    else
        return median(a,a1,low1,m1_i,m2_i,hi2)
    end
end

# Utility function to find median of array
def median(arr,n,lo)
    if n%2==0
        return (arr[lo+(n/2)]+arr[lo+((n/2)-1)])/2,lo+(n/2)
    else
        return arr[lo+(n/2)],lo+(n/2)
    end
end

median_func([1,2,3,5,5],[1,2,5,9,11],0,4,0,4) # => 4 