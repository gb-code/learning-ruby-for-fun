# -----------------------------------------------------------------------
# File Name     	: graph depth first search.rb
# Created By     	: Gaurav Belwal
#Description   		: The program implements depth first searching of graph.
# -----------------------------------------------------------------------


class DFS

  attr_reader :graph
  def set(graph, start)
    @graph = graph
    @start = start
    @visited = []
    @edge_to = {}

    dfs(@graph, @start)
  end

  def dfs(graph, start)
    @visited << start

    @graph[start].each do |node|
      if !@visited.include?(node) #if the node is not visited, make the recursive call from this node
        dfs(graph, node) #recursive call
        @edge_to[node] = start #for all the vertices adjacent to this node
      end
    end
  end

  #all the helper functions below
  def path_to?(visit)
    @visited.include?(visit)
  end

  def trace_path(visit)
    return false unless path_to?(visit)
    path = []

    while visit != @start
      visit = @edge_to[visit]
      path.unshift(visit)
    end

    path.unshift(@start)
    path
  end
end